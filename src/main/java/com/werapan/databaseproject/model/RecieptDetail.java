/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author thisisfocus_k
 */
public class RecieptDetail {
    private int id;
    private int produceId;
    private String produceName;
    private float producePrice;
    private int qty;
    private float totalPrice;
    private int recieptId;

    public RecieptDetail(int id, int produceId, String produceName, float producePrice, int qty, float totalPrice, int recieptId) {
        this.id = id;
        this.produceId = produceId;
        this.produceName = produceName;
        this.producePrice = producePrice;
        this.qty = qty;
        this.totalPrice = totalPrice;
        this.recieptId = recieptId;
    }

    public RecieptDetail(int produceId, String produceName, float producePrice, int qty, float totalPrice, int recieptId) {
        this.id = -1;
        this.produceId = produceId;
        this.produceName = produceName;
        this.producePrice = producePrice;
        this.qty = qty;
        this.totalPrice = totalPrice;
        this.recieptId = recieptId;
    }

    public RecieptDetail() {
        this.id = -1;
        this.produceId = 0;
        this.produceName = "";
        this.producePrice = 0;
        this.qty = 0;
        this.totalPrice = 0;
        this.recieptId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProduceId() {
        return produceId;
    }

    public void setProduceId(int produceId) {
        this.produceId = produceId;
    }

    public String getProduceName() {
        return produceName;
    }

    public void setProduceName(String produceName) {
        this.produceName = produceName;
    }

    public float getProducePrice() {
        return producePrice;
    }

    public void setProducePrice(float producePrice) {
        this.producePrice = producePrice;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
        totalPrice = qty * producePrice;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getRecieptId() {
        return recieptId;
    }

    public void setRecieptId(int recieptId) {
        this.recieptId = recieptId;
    }

    @Override
    public String toString() {
        return "RecieptDetail{" + "id=" + id + ", produceId=" + produceId + ", produceName=" + produceName + ", producePrice=" + producePrice + ", qty=" + qty + ", totalPrice=" + totalPrice + ", recieptId=" + recieptId + '}';
    }
    
    public static RecieptDetail fromRS(ResultSet rs) {
        RecieptDetail recieptDetail = new RecieptDetail();
        try {
            recieptDetail.setId(rs.getInt("reciept_detail_id"));
            recieptDetail.setProduceId(rs.getInt("produce_id"));
            recieptDetail.setProduceName(rs.getString("produce_name"));
            recieptDetail.setProducePrice(rs.getFloat("produce_price"));
            recieptDetail.setQty(rs.getInt("qty"));
            recieptDetail.setTotalPrice(rs.getFloat("total_price"));
            recieptDetail.setRecieptId(rs.getInt("reciept_id"));
        } catch (SQLException ex) {
            Logger.getLogger(RecieptDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return recieptDetail;
    }
    
    
    
    
}
